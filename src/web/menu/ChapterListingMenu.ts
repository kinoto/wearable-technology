import { Folder } from '../../Data';
import { data } from '../data/data';
import { chapterHref } from '../data/hrefs';
import { ItemDecoration, Menu } from '../Menu';
import { shortNumber } from '../util/shortNumber';
import { getDecorationForChapterType } from './ChaptersMenu';

export function isEmptyFolder(folder: Folder): boolean {
  return folder.children.every(child => child.type === 'folder' && isEmptyFolder(child));
}

export class ChapterListingMenu extends Menu {
  public constructor(urlBase: string, folder?: Folder) {
    super(urlBase);
    if (folder === undefined) {
      folder = data.chapterTree;
    }
    for (const child of folder.children) {
      if (child.type === 'folder') {
        if (isEmptyFolder(child)) {
          continue;
        }
        const handle = this.buildSubMenu(child.displayName, ChapterListingMenu, child)
          .setUrlSegment(child.displayName.replace(/ /g, '-'))
          .setDecoration(ItemDecoration.ICON_FOLDER)
          .build();
        if (child.charsCount !== null) {
          handle.append(`[${shortNumber(child.charsCount)}]`, 'char-count');
        }
      } else {
        if (child.hidden) {
          continue;
        }
        const handle = this.addItem(child.displayName, {
          button: true,
          link: chapterHref(child.htmlRelativePath),
          decoration: getDecorationForChapterType(child.type),
        });
        if (child.isEarlyAccess) {
          handle.prepend('[编写中]');
          handle.addClass('early-access');
        }
        if (child.charsCount !== null) {
          handle.append(`[${shortNumber(child.charsCount)}]`, 'char-count');
        }
      }
    }
  }
}
